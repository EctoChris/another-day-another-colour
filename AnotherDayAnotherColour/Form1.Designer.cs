﻿namespace AnotherDayAnotherColour
{
    partial class ColouringProgram
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.imageInput = new System.Windows.Forms.PictureBox();
            this.imageOutput1 = new System.Windows.Forms.PictureBox();
            this.ProcessButton = new System.Windows.Forms.Button();
            this.widthLabel = new System.Windows.Forms.Label();
            this.heightLabel = new System.Windows.Forms.Label();
            this.imageWidthLabel = new System.Windows.Forms.Label();
            this.imageHeightLabel = new System.Windows.Forms.Label();
            this.analyzeButton = new System.Windows.Forms.Button();
            this.folderNameTextBox = new System.Windows.Forms.TextBox();
            this.enterFolderNameLabel = new System.Windows.Forms.Label();
            this.imageUploadButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.closeButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.whitePanel1 = new AnotherDayAnotherColour.WhitePanel(this.components);
            this.ImportLabel = new System.Windows.Forms.Label();
            this.whitePanel2 = new AnotherDayAnotherColour.WhitePanel(this.components);
            this.whitePanel3 = new AnotherDayAnotherColour.WhitePanel(this.components);
            this.whitePanel4 = new AnotherDayAnotherColour.WhitePanel(this.components);
            this.exportProgressBar = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.whitePanel5 = new AnotherDayAnotherColour.WhitePanel(this.components);
            this.exportLabel = new System.Windows.Forms.Label();
            this.statusLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.imageInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageOutput1)).BeginInit();
            this.panel1.SuspendLayout();
            this.whitePanel1.SuspendLayout();
            this.whitePanel2.SuspendLayout();
            this.whitePanel3.SuspendLayout();
            this.whitePanel4.SuspendLayout();
            this.whitePanel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageInput
            // 
            this.imageInput.BackColor = System.Drawing.Color.White;
            this.imageInput.Location = new System.Drawing.Point(29, 17);
            this.imageInput.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.imageInput.Name = "imageInput";
            this.imageInput.Size = new System.Drawing.Size(276, 276);
            this.imageInput.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageInput.TabIndex = 1;
            this.imageInput.TabStop = false;
            this.imageInput.LoadCompleted += new System.ComponentModel.AsyncCompletedEventHandler(this.imageInput_LoadCompleted);
            this.imageInput.MouseClick += new System.Windows.Forms.MouseEventHandler(this.imageInput_MouseClick);
            // 
            // imageOutput1
            // 
            this.imageOutput1.BackColor = System.Drawing.Color.White;
            this.imageOutput1.Location = new System.Drawing.Point(28, 17);
            this.imageOutput1.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.imageOutput1.Name = "imageOutput1";
            this.imageOutput1.Size = new System.Drawing.Size(273, 276);
            this.imageOutput1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageOutput1.TabIndex = 2;
            this.imageOutput1.TabStop = false;
            // 
            // ProcessButton
            // 
            this.ProcessButton.BackColor = System.Drawing.Color.White;
            this.ProcessButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ProcessButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.ProcessButton.Location = new System.Drawing.Point(397, 13);
            this.ProcessButton.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.ProcessButton.Name = "ProcessButton";
            this.ProcessButton.Size = new System.Drawing.Size(230, 28);
            this.ProcessButton.TabIndex = 3;
            this.ProcessButton.Text = "Process Image";
            this.ProcessButton.UseVisualStyleBackColor = false;
            this.ProcessButton.Click += new System.EventHandler(this.ProcessButton_Click);
            // 
            // widthLabel
            // 
            this.widthLabel.AutoSize = true;
            this.widthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Bold);
            this.widthLabel.Location = new System.Drawing.Point(28, 1064);
            this.widthLabel.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.widthLabel.Name = "widthLabel";
            this.widthLabel.Size = new System.Drawing.Size(63, 20);
            this.widthLabel.TabIndex = 4;
            this.widthLabel.Text = "Width:";
            // 
            // heightLabel
            // 
            this.heightLabel.AutoSize = true;
            this.heightLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Bold);
            this.heightLabel.Location = new System.Drawing.Point(28, 1124);
            this.heightLabel.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.heightLabel.Name = "heightLabel";
            this.heightLabel.Size = new System.Drawing.Size(70, 20);
            this.heightLabel.TabIndex = 5;
            this.heightLabel.Text = "Height:";
            // 
            // imageWidthLabel
            // 
            this.imageWidthLabel.AutoSize = true;
            this.imageWidthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.imageWidthLabel.Location = new System.Drawing.Point(210, 1064);
            this.imageWidthLabel.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.imageWidthLabel.Name = "imageWidthLabel";
            this.imageWidthLabel.Size = new System.Drawing.Size(18, 20);
            this.imageWidthLabel.TabIndex = 6;
            this.imageWidthLabel.Text = "0";
            // 
            // imageHeightLabel
            // 
            this.imageHeightLabel.AutoSize = true;
            this.imageHeightLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.imageHeightLabel.Location = new System.Drawing.Point(210, 1124);
            this.imageHeightLabel.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.imageHeightLabel.Name = "imageHeightLabel";
            this.imageHeightLabel.Size = new System.Drawing.Size(18, 20);
            this.imageHeightLabel.TabIndex = 7;
            this.imageHeightLabel.Text = "0";
            // 
            // analyzeButton
            // 
            this.analyzeButton.BackColor = System.Drawing.Color.SlateBlue;
            this.analyzeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.analyzeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.analyzeButton.ForeColor = System.Drawing.Color.White;
            this.analyzeButton.Location = new System.Drawing.Point(62, 13);
            this.analyzeButton.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.analyzeButton.Name = "analyzeButton";
            this.analyzeButton.Size = new System.Drawing.Size(220, 28);
            this.analyzeButton.TabIndex = 8;
            this.analyzeButton.Text = "Export Objects";
            this.analyzeButton.UseVisualStyleBackColor = false;
            this.analyzeButton.Click += new System.EventHandler(this.analyzeButton_Click);
            // 
            // folderNameTextBox
            // 
            this.folderNameTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.folderNameTextBox.Location = new System.Drawing.Point(121, 22);
            this.folderNameTextBox.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.folderNameTextBox.Name = "folderNameTextBox";
            this.folderNameTextBox.Size = new System.Drawing.Size(221, 25);
            this.folderNameTextBox.TabIndex = 9;
            // 
            // enterFolderNameLabel
            // 
            this.enterFolderNameLabel.AutoSize = true;
            this.enterFolderNameLabel.BackColor = System.Drawing.Color.White;
            this.enterFolderNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F);
            this.enterFolderNameLabel.Location = new System.Drawing.Point(6, 25);
            this.enterFolderNameLabel.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.enterFolderNameLabel.Name = "enterFolderNameLabel";
            this.enterFolderNameLabel.Size = new System.Drawing.Size(115, 22);
            this.enterFolderNameLabel.TabIndex = 10;
            this.enterFolderNameLabel.Text = "Folder name:";
            // 
            // imageUploadButton
            // 
            this.imageUploadButton.BackColor = System.Drawing.Color.SlateBlue;
            this.imageUploadButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.imageUploadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.imageUploadButton.ForeColor = System.Drawing.Color.White;
            this.imageUploadButton.Location = new System.Drawing.Point(392, 21);
            this.imageUploadButton.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.imageUploadButton.Name = "imageUploadButton";
            this.imageUploadButton.Size = new System.Drawing.Size(230, 26);
            this.imageUploadButton.TabIndex = 11;
            this.imageUploadButton.Text = "Click to Upload";
            this.imageUploadButton.UseVisualStyleBackColor = false;
            this.imageUploadButton.Click += new System.EventHandler(this.imageUploadButton_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SlateBlue;
            this.panel1.Controls.Add(this.closeButton);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(708, 60);
            this.panel1.TabIndex = 12;
            // 
            // closeButton
            // 
            this.closeButton.BackColor = System.Drawing.Color.White;
            this.closeButton.BackgroundImage = global::AnotherDayAnotherColour.Properties.Resources.xButton;
            this.closeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.closeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.closeButton.Location = new System.Drawing.Point(662, 12);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(34, 34);
            this.closeButton.TabIndex = 2;
            this.closeButton.UseVisualStyleBackColor = false;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(193, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(307, 26);
            this.label2.TabIndex = 1;
            this.label2.Text = "\"Another Day, Another Colour\"";
            // 
            // whitePanel1
            // 
            this.whitePanel1.Controls.Add(this.ImportLabel);
            this.whitePanel1.Controls.Add(this.enterFolderNameLabel);
            this.whitePanel1.Controls.Add(this.folderNameTextBox);
            this.whitePanel1.Controls.Add(this.imageUploadButton);
            this.whitePanel1.Location = new System.Drawing.Point(17, 70);
            this.whitePanel1.Name = "whitePanel1";
            this.whitePanel1.Size = new System.Drawing.Size(674, 58);
            this.whitePanel1.TabIndex = 14;
            // 
            // ImportLabel
            // 
            this.ImportLabel.AutoSize = true;
            this.ImportLabel.BackColor = System.Drawing.Color.White;
            this.ImportLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.ImportLabel.Location = new System.Drawing.Point(7, 4);
            this.ImportLabel.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.ImportLabel.Name = "ImportLabel";
            this.ImportLabel.Size = new System.Drawing.Size(42, 13);
            this.ImportLabel.TabIndex = 11;
            this.ImportLabel.Text = "Import";
            // 
            // whitePanel2
            // 
            this.whitePanel2.Controls.Add(this.imageInput);
            this.whitePanel2.Location = new System.Drawing.Point(14, 145);
            this.whitePanel2.Name = "whitePanel2";
            this.whitePanel2.Size = new System.Drawing.Size(330, 310);
            this.whitePanel2.TabIndex = 15;
            // 
            // whitePanel3
            // 
            this.whitePanel3.Controls.Add(this.imageOutput1);
            this.whitePanel3.Location = new System.Drawing.Point(364, 145);
            this.whitePanel3.Name = "whitePanel3";
            this.whitePanel3.Size = new System.Drawing.Size(327, 310);
            this.whitePanel3.TabIndex = 16;
            // 
            // whitePanel4
            // 
            this.whitePanel4.Controls.Add(this.statusLabel);
            this.whitePanel4.Controls.Add(this.exportProgressBar);
            this.whitePanel4.Controls.Add(this.label1);
            this.whitePanel4.Location = new System.Drawing.Point(12, 461);
            this.whitePanel4.Name = "whitePanel4";
            this.whitePanel4.Size = new System.Drawing.Size(679, 83);
            this.whitePanel4.TabIndex = 16;
            // 
            // exportProgressBar
            // 
            this.exportProgressBar.Location = new System.Drawing.Point(31, 30);
            this.exportProgressBar.Name = "exportProgressBar";
            this.exportProgressBar.Size = new System.Drawing.Size(622, 20);
            this.exportProgressBar.Step = 1;
            this.exportProgressBar.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(6, 6);
            this.label1.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Progress";
            // 
            // whitePanel5
            // 
            this.whitePanel5.Controls.Add(this.exportLabel);
            this.whitePanel5.Controls.Add(this.ProcessButton);
            this.whitePanel5.Controls.Add(this.analyzeButton);
            this.whitePanel5.Location = new System.Drawing.Point(12, 550);
            this.whitePanel5.Name = "whitePanel5";
            this.whitePanel5.Size = new System.Drawing.Size(679, 55);
            this.whitePanel5.TabIndex = 17;
            // 
            // exportLabel
            // 
            this.exportLabel.AutoSize = true;
            this.exportLabel.BackColor = System.Drawing.Color.White;
            this.exportLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.exportLabel.Location = new System.Drawing.Point(5, 4);
            this.exportLabel.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.exportLabel.Name = "exportLabel";
            this.exportLabel.Size = new System.Drawing.Size(43, 13);
            this.exportLabel.TabIndex = 13;
            this.exportLabel.Text = "Export";
            // 
            // statusLabel
            // 
            this.statusLabel.AutoSize = true;
            this.statusLabel.BackColor = System.Drawing.Color.White;
            this.statusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.statusLabel.Location = new System.Drawing.Point(260, 57);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(155, 16);
            this.statusLabel.TabIndex = 14;
            this.statusLabel.Text = "Please upload an image";
            // 
            // ColouringProgram
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 26F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(708, 611);
            this.Controls.Add(this.whitePanel5);
            this.Controls.Add(this.whitePanel4);
            this.Controls.Add(this.whitePanel3);
            this.Controls.Add(this.whitePanel2);
            this.Controls.Add(this.whitePanel1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.imageHeightLabel);
            this.Controls.Add(this.imageWidthLabel);
            this.Controls.Add(this.heightLabel);
            this.Controls.Add(this.widthLabel);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.ForeColor = System.Drawing.Color.SlateBlue;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.Name = "ColouringProgram";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Another Day Another Colour";
            ((System.ComponentModel.ISupportInitialize)(this.imageInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageOutput1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.whitePanel1.ResumeLayout(false);
            this.whitePanel1.PerformLayout();
            this.whitePanel2.ResumeLayout(false);
            this.whitePanel3.ResumeLayout(false);
            this.whitePanel4.ResumeLayout(false);
            this.whitePanel4.PerformLayout();
            this.whitePanel5.ResumeLayout(false);
            this.whitePanel5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox imageInput;
        private System.Windows.Forms.PictureBox imageOutput1;
        private System.Windows.Forms.Button ProcessButton;
        private System.Windows.Forms.Label widthLabel;
        private System.Windows.Forms.Label heightLabel;
        private System.Windows.Forms.Label imageWidthLabel;
        private System.Windows.Forms.Label imageHeightLabel;
        private System.Windows.Forms.Button analyzeButton;
        private System.Windows.Forms.TextBox folderNameTextBox;
        private System.Windows.Forms.Label enterFolderNameLabel;
        private System.Windows.Forms.Button imageUploadButton;
        private System.Windows.Forms.Panel panel1;
        private WhitePanel whitePanel1;
        private System.Windows.Forms.Label ImportLabel;
        private WhitePanel whitePanel2;
        private WhitePanel whitePanel3;
        private WhitePanel whitePanel4;
        private WhitePanel whitePanel5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label exportLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ProgressBar exportProgressBar;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.Label statusLabel;
    }
}

