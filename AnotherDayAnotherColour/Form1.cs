﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System.Threading;
using System.IO;
using System.Drawing.Imaging;

namespace AnotherDayAnotherColour
{
    public partial class ColouringProgram : Form
    {
        public Image<Bgr, byte> imageObject;
        public Image<Bgr, byte> tempImageObject;
        public Image<Bgra, byte> tempTransparentImageObject;
        public Image<Gray, byte> blankImageObject;
        public VectorOfVectorOfPoint objectDetectionContours = new VectorOfVectorOfPoint();
        public static string desktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

        public ColouringProgram()
        {
            InitializeComponent();
            // Ensure WaitOnLoad is false.
            ////imageOutput1.WaitOnLoad = false;
        }

        private void detectObjects()
        {
            //Perform object detection: 230 old
            var objectDetectionTemp = imageObject.SmoothGaussian(5).Convert<Gray, byte>().ThresholdBinaryInv(new Gray(245), new Gray(255));
            Mat m1 = new Mat();
            objectDetectionContours = new VectorOfVectorOfPoint();

            CvInvoke.FindContours(objectDetectionTemp, objectDetectionContours, m1, Emgu.CV.CvEnum.RetrType.External, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple);

            for (int i = 0; i < objectDetectionContours.Size; i++)
            {
                double perimeter = CvInvoke.ArcLength(objectDetectionContours[i], true);
                VectorOfPoint approx = new VectorOfPoint();
                CvInvoke.ApproxPolyDP(objectDetectionContours[i], approx, 0.01 * perimeter, true);
                imageInput.Image = imageObject.Bitmap;
            }
            //CvInvoke.DrawContours(imageObject, objectDetectionContours, -1, new MCvScalar(255, 0, 0), 12);
            imageInput.Image = imageObject.Bitmap;
        }

        private void exportContour(VectorOfPoint contour, int imageNo)
        {
            //Make a mask with same dimensions:
            Image<Bgr, byte> blankImage = imageObject.CopyBlank();

            //Set Mask to black:
            Image<Gray, byte> whiteMask = blankImage.Convert<Gray, byte>().ThresholdBinaryInv(new Gray(255), new Gray(255));

            //Draw White Poly on Mask:
            VectorOfVectorOfPoint vList = new VectorOfVectorOfPoint();
            vList.Push(contour);
            CvInvoke.FillPoly(whiteMask, vList, new MCvScalar(0, 0, 0));

            //Crop image to bounding box of contour:
            Rectangle boundingBox = CvInvoke.BoundingRectangle(contour);
            Size s = new Size(Convert.ToInt32(boundingBox.Width * 0.03), Convert.ToInt32(boundingBox.Height * 0.03));
            boundingBox.Inflate(s);

            imageObject.ROI = boundingBox;
            blankImage.ROI = boundingBox;
            whiteMask.ROI = boundingBox;
  
            //Turn white mask into black:
            Image<Gray, byte> inverse = whiteMask.Not();
            Image<Bgr, byte> image = imageObject.Copy(inverse);

            //Set black mask to transparent:
            Image<Bgra, byte> transparent = BlackTransparent(image);
          
            //imageOutput1.WaitOnLoad = true;
            //imageOutput1.Image = transparent.Bitmap;
            //imageOutput1.Refresh();
            //Thread.Sleep(1000);

            //Turn into colouring pic:
            this.turnIntoColouringPic(transparent, imageNo);
            //test.ROI = Rectangle.Empty; //here
            imageObject.ROI = Rectangle.Empty;
        }

        private async void analyzeButton_Click(object sender, EventArgs e)
        {
            exportProgressBar.Maximum = objectDetectionContours.Size * 10;
            for (int i = 0; i < objectDetectionContours.Size; i++)
            {
                this.exportContour(objectDetectionContours[i], i);
                exportProgressBar.Invoke(new Action(() =>
                {
                    exportProgressBar.Increment(10);
                }));
                statusLabel.Invoke(new Action(() =>
                {
                    statusLabel.Text = "Analyzing object No: " + (i+1) + " / " + objectDetectionContours.Size;
                    statusLabel.Refresh();
                }));
            }
            MessageBox.Show("Finished Outputting Objects");
        }

        private async void ProcessButton_Click(object sender, EventArgs e)
        {
            
            try {
                tempImageObject = imageObject.CopyBlank();
                blankImageObject = tempImageObject.Convert<Gray, byte>().ThresholdBinaryInv(new Gray(254), new Gray(255));
                int minimumThreshold = 250;

                VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();

                int counter = 0;
                while (minimumThreshold > 0)
                {
                    exportProgressBar.Value = 0;
                    exportProgressBar.Refresh();
                    var temp = imageObject.SmoothGaussian(5).Convert<Gray, byte>().ThresholdBinaryInv(new Gray(minimumThreshold), new Gray(255));
                    Mat m = new Mat();

                    CvInvoke.FindContours(temp, contours, m, Emgu.CV.CvEnum.RetrType.List, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple);
                    exportProgressBar.Maximum = contours.Size * 10;
                    exportProgressBar.Step = (int)100 / contours.Size;
                    for (int i = 0; i < contours.Size; i++)
                    {
                        
                        CvInvoke.DrawContours(blankImageObject, contours, -1, new MCvScalar(0, 0, 0), 20); //3 = original

                        exportProgressBar.Invoke(new Action(() =>
                        {
                            exportProgressBar.Increment(10);
                        }));

                        statusLabel.Invoke(new Action(() =>
                        {
                            statusLabel.Text = "Threshold Analysis No: " + (counter+1) + "/ 13, Analyzing Contour " + i + " / " + contours.Size;
                            statusLabel.Refresh();
                        }));
                    }
                    counter++;

                    minimumThreshold -= 37; //original 20
                    Console.WriteLine("Counter is at : " + counter);
                }
                imageOutput1.WaitOnLoad = true;
                imageOutput1.Image = blankImageObject.Bitmap;
                this.downloadWholePic(blankImageObject);
                statusLabel.Invoke(new Action(() =>
                {
                    statusLabel.Text = "Finished analyzing image, downloaded to desktop";
                    statusLabel.Refresh();
                }));
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
    
            }
        }
        private void downloadWholePic(Image<Gray, byte> image)
        {
            try
            {
                if (folderNameTextBox.Text.Length > 0)
                {
                    System.IO.Directory.CreateDirectory(desktop + @"\" + folderNameTextBox.Text);
                    string filePath = desktop + @"\" + folderNameTextBox.Text + @"\";

                    using (MemoryStream memory = new MemoryStream())
                    {
                        using (FileStream fs = new FileStream(filePath + folderNameTextBox.Text + ".jpg", FileMode.Create, FileAccess.ReadWrite))
                        {
                            blankImageObject.Bitmap.Save(memory, ImageFormat.Jpeg);
                            byte[] bytes = memory.ToArray();
                            fs.Write(bytes, 0, bytes.Length);
                        }
                    }
                }
            } catch(Exception x)
            {
                //MessageBox.Show(x.Message);
            }
        }
        private void turnIntoColouringPic(Image<Bgra, byte> croppedImage, int imageNo)
        {
            try
            {
                tempTransparentImageObject = croppedImage.CopyBlank();
                blankImageObject = tempTransparentImageObject.Convert<Gray, byte>().ThresholdBinaryInv(new Gray(254), new Gray(255));
                int minimumThreshold = 250;

                VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();

                int counter = 0;
                while (minimumThreshold > 0)
                {
                    var temp = croppedImage.SmoothGaussian(5).Convert<Gray, byte>().ThresholdBinaryInv(new Gray(minimumThreshold), new Gray(255));
                    Mat m = new Mat();

                    CvInvoke.FindContours(temp, contours, m, Emgu.CV.CvEnum.RetrType.List, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple);
                    for (int i = 0; i < contours.Size; i++)
                    {
                        CvInvoke.DrawContours(blankImageObject, contours, -1, new MCvScalar(0, 0, 0), 3);
                    }
                    counter++;

                    minimumThreshold -= 20;
                }

                blankImageObject.ROI = new Rectangle(3, 3, blankImageObject.Width - 6, blankImageObject.Height - 6);
                imageOutput1.WaitOnLoad = true;
                imageOutput1.Image = blankImageObject.Bitmap;
                imageOutput1.Refresh();
                //Thread.Sleep(2000);

                if(folderNameTextBox.Text.Length > 0)
                {
                    System.IO.Directory.CreateDirectory(desktop + @"\" + folderNameTextBox.Text);
                    string filePath = desktop + @"\" + folderNameTextBox.Text + @"\";

                    using (MemoryStream memory = new MemoryStream())
                    {
                        using (FileStream fs = new FileStream(filePath + folderNameTextBox.Text + imageNo + ".jpg", FileMode.Create, FileAccess.ReadWrite))
                        {
                            blankImageObject.Bitmap.Save(memory, ImageFormat.Jpeg);
                            byte[] bytes = memory.ToArray();
                            fs.Write(bytes, 0, bytes.Length);
                        }
                    }
                    statusLabel.Invoke(new Action(() =>
                    {
                        statusLabel.Text = "Downloaded to desktop";
                        statusLabel.Refresh();
                    }));
                }
            } catch(Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void imageInput_LoadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            this.detectObjects();
            //this.detectObjectsWithOtsu();
        }

        private void imageInput_MouseClick(object sender, MouseEventArgs e)
        {
            Console.WriteLine("Mouse X: " + e.X);
            Console.WriteLine("Mouse Y: " + e.Y);
        }

        public Image<Bgra, byte> BlackTransparent(Image<Bgr, byte> image)
        {
            Mat imageMat = image.Mat;
            Mat finalMat = new Mat(imageMat.Rows, imageMat.Cols, Emgu.CV.CvEnum.DepthType.Cv8U, 4);
            Mat tmp = new Mat(imageMat.Rows, imageMat.Cols, Emgu.CV.CvEnum.DepthType.Cv8U, 1);
            Mat alpha = new Mat(imageMat.Rows, imageMat.Cols, Emgu.CV.CvEnum.DepthType.Cv8U, 1);
            CvInvoke.CvtColor(imageMat, tmp, Emgu.CV.CvEnum.ColorConversion.Bgr2Gray);
            CvInvoke.Threshold(tmp, alpha, 0, 255, Emgu.CV.CvEnum.ThresholdType.Binary);
            VectorOfMat rgb = new VectorOfMat(3);
            CvInvoke.Split(imageMat, rgb);
            Mat[] rgba = { rgb[0], rgb[1], rgb[2], alpha };
            VectorOfMat vector = new VectorOfMat(rgba);
            CvInvoke.Merge(vector, finalMat);
            //Opening:
            Mat kernel = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Rectangle, new Size(5, 5), new Point(-1, -1));
            Image<Bgra, byte> testImage = finalMat.ToImage<Bgra,byte>().MorphologyEx(Emgu.CV.CvEnum.MorphOp.Open, kernel, new Point(-1, -1), 1, Emgu.CV.CvEnum.BorderType.Default, new MCvScalar(2.0));

            //return finalMat.ToImage<Bgra, byte>();
            return testImage;
        }
        private void imageUploadButton_Click(object sender, EventArgs e)
        {
            string imageLocation = "";
            try
            {
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";

                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    imageLocation = dialog.FileName;
                    imageInput.ImageLocation = imageLocation;
                    imageObject = new Image<Bgr, byte>(dialog.FileName);

                    imageHeightLabel.Text = imageObject.Height.ToString();
                    imageWidthLabel.Text = imageObject.Width.ToString();
                    Console.WriteLine("The width of imageInput: " + imageInput.Width);
                    Console.WriteLine("The height of imageInput: " + imageInput.Height);
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
