﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnotherDayAnotherColour
{
    public partial class WhitePanel : Panel
    {
        [Category("Custom")]
        [Browsable(true)]
        [Editor(typeof(System.Windows.Forms.Design.WindowsFormsComponentEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public WhitePanel()
        {
            InitializeComponent();
        }

        public WhitePanel(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            //Graphics g = e.Graphics;
            //g.SmoothingMode = SmoothingMode.AntiAlias;
            //g.FillRoundedRectangle(new SolidBrush(Color.White), 10, 10, this.Width - 40, this.Height - 60, 10);
            //SolidBrush brush = new SolidBrush(
            //    Color.White
            //    );
            //g.FillRoundedRectangle(brush, 12, 12, this.Width - 44, this.Height - 64, 10);
            //g.DrawRoundedRectangle(new Pen(ControlPaint.Light(Color.White, 0.00f)), 12, 12, this.Width - 44, this.Height - 64, 10);
            //g.FillRoundedRectangle(new SolidBrush(Color.White), 12, 12 + ((this.Height - 64) / 2), this.Width - 44, (this.Height - 64) / 2, 10);
            Graphics g = e.Graphics;
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.FillRoundedRectangle(new SolidBrush(Color.White), 0, 0, this.Width - 10, this.Height - 10, 15);
            SolidBrush brush = new SolidBrush(
                Color.White
                );
            g.FillRoundedRectangle(brush, 0, 0, this.Width, this.Height, 15);
            g.DrawRoundedRectangle(new Pen(ControlPaint.Light(Color.White, 0.00f)), 0, 0, this.Width, this.Height, 15);
            g.FillRoundedRectangle(new SolidBrush(Color.White), 0, 0 + ((this.Height) / 2), this.Width, (this.Height) / 2, 15);
        }
    }
}
